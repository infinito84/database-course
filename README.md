# Database Course

## Start

- Install [docker](https://docs.docker.com/engine/install/) 
- Open terminal on this folder and run: `docker-compose up`
- Open in the browser: `http://localhost:7000`
  - User: dbeaver
  - Password: Admin1234
- For finish Ctrl+C in the terminal

This automatically creates dbeaver, mysql-data, postgres-data, if you want to delete the data can remove this folder.

### Notes for oracle:

- For create an schema you should run:
  - `create user xxx identified by xxx;`
  - where `xxx` it's the name of the schema
  - after that disconnect and connect again and you can create tables on that schema.

## Diagrams

Create account: https://erdplus.com/

